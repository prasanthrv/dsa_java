package com.dare.dsa.basic;

import java.util.Iterator;

/* A Queue implmentation using doubly linked list */
public class Queue<V> implements Iterable<V> {
    DblLinkedList<V> list;

    public Queue() {
        list = new DblLinkedList<>();
    }

    public void enqueue(V item) {
        list.insertBack(item);
    }

    public V dequeue() {
        return list.removeFront();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return list.getSize();
    }

    @Override
    public Iterator<V> iterator() {
        return list.iterator();
    }
}
