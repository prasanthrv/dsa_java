package com.dare.dsa.basic;

import java.util.Iterator;

/**
 * A doubly linked list that allows insertion
 * and removal at both ends
 * @param <V> Type of values
 */
class DblLinkedList<V> implements Iterable<V> {
    private DoubleNode<V> head;
    private DoubleNode<V> tail;
    private int size;

    DblLinkedList() {
        head = tail = null;
        size = 0;
    }

    int getSize() {
        return size;
    }

    /* insert in front */
    public void insertFront(V item) {
        DoubleNode<V> temp;
        if (head == null) {
            temp = new DoubleNode<V>(item, null, null);
            head = tail = temp;
        } else {
            temp = new DoubleNode<V>(item,head,null);
            head.prev = temp;
            head = temp;
        }
        size++;
    }

    /* insert at back */
    void insertBack(V item) {
        DoubleNode<V> temp;
        if (tail == null) {
            temp = new DoubleNode<V>(item, null, null);
            head = tail = temp;
        } else {
            temp = new DoubleNode<V>(item,null,tail);
            tail.next = temp;
            tail = temp;
        }
        size++;
    }

    /* remove from front */
    V removeFront() {
        if (head == null) {
            return null;
        } else {
            V item = head.getValue();
            head = head.next;
            size--;
            return item;
        }
    }

    /* remove at back */
    V removeBack() {
        if (tail == null) {
            return null;
        } else {
            V item = tail.getValue();
            tail = tail.prev;
            size--;
            return item;
        }
    }


    /* an iterator to iterate from front to back */
    @Override
    public Iterator<V> iterator() {
        return new Iterator<V>() {
            DoubleNode<V> cur = head;
            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public V next() {
                V item = cur.getValue();
                cur = cur.next;
                return item;
            }
        };
    }
}
