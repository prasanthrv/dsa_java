package com.dare.dsa.basic;

import java.util.Iterator;

/**
 * A linked list implementation
 * that allows insertion and removal from front
 * @param <V>
 */
class LinkedList<V> implements Iterable<V>{

    private SingleNode<V> head;
    private int size;

    LinkedList() {
        this.head = null;
        size = 0;
    }

    void insert(V item) {
        head = new SingleNode<>(item, head);
        size++;
    }

    V remove() {
       if (head == null) {
           return null;
       }
        V item = head.getItem();
        head = head.next;
        size--;
        return item;
    }

    int getSize() {
        return size;
    }

    /* iteration from front to back */
    @Override
    public Iterator<V> iterator() {
        return new Iterator<V>() {
            SingleNode<V> curNode = head;
            @Override
            public boolean hasNext() {
                return curNode != null;
            }

            @Override
            public V next() {
                V curItem = curNode.getItem();
                curNode = curNode.next;
                return curItem;
            }
        };
    }
}
