package com.dare.dsa.basic;

import java.util.Iterator;

/* A stack implementation using single linked list */
public class Stack<V>  implements Iterable<V> {
    private LinkedList<V> list;

    public Stack() {
        list = new LinkedList<>();
    }

    public void push(V item) {
        list.insert(item);
    }

    public V pop() {
        return list.remove();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return list.getSize();
    }

    @Override
    public Iterator<V> iterator() {
        return list.iterator();
    }
}
