package com.dare.dsa.basic;

class DoubleNode<V> {

    private V value;
    DoubleNode<V> next;
    DoubleNode<V> prev;

    public DoubleNode(V value, DoubleNode<V> next, DoubleNode<V> prev) {
        this.value = value;
        this.next = next;
        this.prev = prev;
    }

    public V getValue() {
        return value;
    }
}
