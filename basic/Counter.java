package com.dare.dsa.basic;

/**
 * A simple counter class,
 * that can increment value, but not decrement
 */
class Counter {

    private String name;
    private int value;

    public Counter(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public Counter(String name) {
        this.name = name;
        value = 0;
    }

    public void increment() {
        value++;
    }

    public int tally() {
        return value;
    }
}
