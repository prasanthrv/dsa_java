package com.dare.dsa.basic;

/** A single node with link to next node at back
 * @param <V> Value type
 */
class SingleNode<V> {

    private V item;
    SingleNode<V> next;

    public SingleNode(V item, SingleNode next) {
        this.item = item;
        this.next = next;
    }

    public V getItem() {
        return item;
    }

}
