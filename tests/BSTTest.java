package com.dare.dsa.tests;

import com.dare.dsa.Util;
import com.dare.dsa.store.BasicBST;

import static com.dare.dsa.Util.*;
import static org.junit.Assert.*;

public class BSTTest {
    private final int arraySize = 10000;
    private final int minElem = 0;
    private final int maxElem = arraySize * 4;
    private final Integer[] rndInts =  getRandomIntegers(minElem, maxElem, arraySize);
    private final Integer[] arr = {5, 6, 1, 4, 3, 1};

    @org.junit.Test
    public void searchAndInsertBST() throws Exception {
        BasicBST<Integer, Integer> bst = new BasicBST<>();

        for (Integer i: rndInts) {
            bst.insert(i,i);
        }

        for (Integer i: rndInts) {
            assertEquals(i, bst.search(i));
        }

        assertTrue(isSorted(bst.traversal.inOrderTraversal()));

    }

    @org.junit.Test
    public void floorTest() throws Exception {
        BasicBST<Integer, Integer> bst = new BasicBST<>();

        for (Integer i: arr) {
            bst.insert(i,i);
        }

        assertTrue(bst.floor(6) == 6);
        assertTrue(bst.floor(7) == 6);
        assertTrue(bst.floor(8) == 6);
        assertTrue(bst.floor(2) == 1);
        assertEquals(bst.floor(0), null);
    }

    @org.junit.Test
    public void ceilTest() throws Exception {
        BasicBST<Integer, Integer> bst = new BasicBST<>();

        for (Integer i: arr) {
            bst.insert(i,i);
        }

        assertTrue(bst.ceil(1) == 1);
        assertTrue(bst.ceil(2) == 3);
        assertEquals(bst.ceil(7), null);
    }

    @org.junit.Test
    public void rankTest() throws Exception {
        BasicBST<Integer, Integer> bst = new BasicBST<>();

        for (Integer i: arr) {
            bst.insert(i,i);
        }

        assertTrue(bst.rank(1) == 1);
        assertTrue(bst.rank(4) == 3);
        assertTrue(bst.rank(2) == 2);
    }


}