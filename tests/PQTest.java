package com.dare.dsa.tests;

import com.dare.dsa.store.MaxPQ;
import com.dare.dsa.store.MinPQ;

import static com.dare.dsa.Util.*;
import static org.junit.Assert.*;

public class PQTest {
    private final int arraySize = 100000;
    private final int minElem = 0;
    private final int maxElem = arraySize * 2;
    private final Integer[] rndInts =  getRandomIntegers(minElem, maxElem, arraySize);

    @org.junit.Test
    public void minPQSort() throws Exception {
        MinPQ pq = new MinPQ(arraySize + 1);
        Integer[] newInts = new Integer[arraySize];

        for (Integer i: rndInts) {
            pq.insert(i);
        }

        for (int i = 0; i< arraySize; i++) {
            newInts[i]  = (Integer) pq.delMin();
        }

        assertTrue(isSorted(1,newInts));
    }

    @org.junit.Test
    public void maxPQSort() throws Exception {
        MaxPQ pq = new MaxPQ(arraySize + 1);
        Integer[] newInts = new Integer[arraySize];

        for (Integer i: rndInts) {
            pq.insert(i);
        }

        for (int i = 0; i< arraySize; i++) {
            newInts[i]  = (Integer) pq.delMax();
        }

        Integer[] revInts = reverseArray(newInts);
        assertTrue(isSorted(1, revInts));
    }

}