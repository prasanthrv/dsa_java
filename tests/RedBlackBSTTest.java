package com.dare.dsa.tests;

import com.dare.dsa.store.BasicBST;
import com.dare.dsa.store.RedBlackBST;

import static com.dare.dsa.Util.getRandomIntegers;
import static com.dare.dsa.Util.isSorted;
import static org.junit.Assert.*;

public class RedBlackBSTTest {
    private final int arraySize = 10000;
    private final int minElem = 0;
    private final int maxElem = arraySize * 4;
    private final Integer[] rndInts =  getRandomIntegers(minElem, maxElem, arraySize);
    private final Integer[] arr = {5, 6, 1, 4, 3, 1};

    @org.junit.Test
    public void searchAndInsertBST() throws Exception {
        BasicBST<Integer, Integer> bst = new BasicBST<>();

        for (int i=0; i<arraySize; i++) {
            bst.insert(i,i);
        }

        for (Integer i=0; i<arraySize; i++) {
            assertEquals(i, bst.search(i));
        }

        assertTrue(isSorted(bst.traversal.inOrderTraversal()));

    }

     @org.junit.Test
    public void searchAndInsertRedBlackBST() throws Exception {
        RedBlackBST<Integer, Integer> bst = new RedBlackBST<>();

        for (int i=0; i<arraySize; i++) {
            bst.insert(i,i);
        }

        for (Integer i=0; i<arraySize; i++) {
            assertEquals(i, bst.search(i));
        }

        RedBlackBST<Integer, Integer> rbBst = new RedBlackBST<>();



    }

}