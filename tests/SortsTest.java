package com.dare.dsa.tests;

import com.dare.dsa.Sorts;
import org.junit.Test;

import static com.dare.dsa.Sorts.*;
import static org.junit.Assert.*;
import static com.dare.dsa.Util.*;

public class SortsTest {
    private final int arraySize = 10000;
    private final int minElem = 0;
    private final int maxElem = arraySize * 2;
    private final Integer[] rndInts =  getRandomIntegers(minElem, maxElem, arraySize);

    @org.junit.Test
    public void bubbleSort() throws Exception {
        Integer[] ints = rndInts.clone();
        timeSortMethod("Bubble Sort", () -> Sorts.bubbleSort(ints));
        assertTrue(isSorted(ints));
    }

    @org.junit.Test
    public void selectionSort() throws Exception {
        Integer[] ints = rndInts.clone();
        timeSortMethod("Selection Sort", () -> Sorts.selectionSort(ints));
        assertTrue(isSorted(ints));
    }

    @org.junit.Test
    public void insertionSort() throws Exception {
        Integer[] ints = rndInts.clone();
        timeSortMethod("Insertion Sort", () -> Sorts.insertionSort(ints));
        assertTrue(isSorted(ints));
    }

    @org.junit.Test
    public void mergeSort() throws Exception {
        Integer[] ints = rndInts.clone();
        timeSortMethod("Merge Sort", () -> Sorts.mergeSort(ints));
        assertTrue(isSorted(ints));
    }

    @org.junit.Test
    public void quickSort() throws Exception {
        Integer[] ints = rndInts.clone();
        timeSortMethod("Quick Sort", () -> Sorts.quickSort(ints));
        assertTrue(isSorted(ints));
    }

    @Test
    public void rankMethods() throws Exception {
        Integer[] arr = {5, 8, 9 , 1, 3};

        assertEquals(5, Sorts.getRankedItem(arr, 3));
        assertEquals(4, Sorts.getRank(arr, 8));
    }

    @Test
    public void uniqueElemsTest() throws Exception {
        Integer[] arr = {5, 8, 8, 9, 8, 1, 3, 9, 1, 9, 5};
        Integer[] array = getRandomIntegers(0, 5000, 10000);

        int unqItems = uniqueElems(arr);
        System.out.println("Unique items: " + unqItems);
        assertEquals(unqItems, 5);
    }

}