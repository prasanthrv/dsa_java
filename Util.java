package com.dare.dsa;

import java.util.Iterator;
import java.util.Random;


/** misc functions needed for DSA */
public class Util {

    /* runs a Runnable and logs time taken */
    public static void timeSortMethod( String functionName, Runnable fn) {
        System.out.println("Running " + functionName);
        long startTime = System.currentTimeMillis();
        fn.run();
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime) + " ms");
    }


    /* Generate random Integers */
    public static Integer[] getRandomIntegers(int min, int max, int capacity) {
        Integer[] items = new Integer[capacity];

        Random rnd = new Random(System.currentTimeMillis());
        for (int i=0;i< capacity; i++) {
            items[i] = min + rnd.nextInt(max - min + 1);
        }
        return items;
    }

    /* compare functions */
    public static boolean lesserOrEqual(Comparable a, Comparable b) {
        return  a.compareTo(b) <= 0;
    }

    public static boolean lesser(Comparable a, Comparable b) {
       return a.compareTo(b) < 0;
    }

    public static boolean greater(Comparable a, Comparable b) {
        return a.compareTo(b) > 0;
    }

    public static boolean equal(Comparable a, Comparable b) {
       return a.compareTo(b) == 0;
    }

    /* swap two objects in array */
    public static void swap(Object[] arr, int i, int j) {
        if (i == j) return;
        Object tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    /* print an array of objects */
    public static void printArray(Object[] objs) {
        for (Object obj : objs) {
            System.out.println(obj);
        }
    }

    /* check if array is sorted */
    public static boolean isSorted(Comparable[] items) {
        return isSorted(0, items);
    }

    /* check if container is sorted */
    public static <V extends Comparable> boolean isSorted(Iterable<V> items) {

        boolean isSorted = true;
        V itemPrev, item;
        Iterator<V> iter = items.iterator();

        if (!iter.hasNext()) return true;
        else itemPrev = iter.next();

        while (iter.hasNext()) {
            item = iter.next();
            if (!lesserOrEqual(itemPrev, item)) {
                isSorted = false;
                break;
            }
        }

        return isSorted;

    }

    public static boolean isSorted(int start, Comparable[] items) {
        boolean isSorted = true;
        for (int i =start; i< items.length - 1; i++) {
           if (!lesserOrEqual(items[i], items[i+1])) {
               isSorted = false;
               break;
           }
        }
        return isSorted;
    }

    /* reverse an array */
    public static <V>  V[] reverseArray(V[] array) {
        for (int i=0, j = array.length - 1; i < j; i++, j--) {
            swap(array, i, j);
        }
        return array;
    }

}
