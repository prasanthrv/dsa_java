package com.dare.dsa.store;
import static com.dare.dsa.Util.*;
import com.dare.dsa.basic.Queue;

public class RedBlackBST<K extends Comparable, V> implements SymbolTable<K,V>{

    private RedBlackNode<K,V> root;
    private static final boolean BLACK = false;
    private static final boolean RED   = true;

    class RedBlackNode<K extends Comparable,V> {

        RedBlackNode<K,V> left;
        RedBlackNode<K,V> right;
        V val;
        K key;
        int size;


        boolean color;
        public RedBlackNode(K key, V val) {
            this.key = key;
            this.val = val;
            this.size = 1;
            this.color = RED;
        }


    public K getKey() {
        return key;
    }


    }

    /* helper method for getting size, 0 if null */
    public int getSize(RedBlackNode<K,V> node) {
        if (node == null) { return 0;         }
        else              { return node.size; }
    }

    private RedBlackNode<K,V> leftRotate(RedBlackNode<K,V> node) {
        RedBlackNode<K,V> right = node.right;
        node.right = right.left;
        right.left = node;
        right.color = node.color;
        node.color = RED;
        return right;
    }

    private RedBlackNode<K,V> rightRotate(RedBlackNode<K,V> node) {
        RedBlackNode<K,V> left = node.left;
        node.left = left.right;
        left.right = node;
        left.color = node.color;
        node.color = RED;
        return left;
    }

    private void flipColors(RedBlackNode<K,V> node) {
        node.left.color = BLACK;
        node.right.color = BLACK;
        node.color = RED;
    }

    private boolean isRed(RedBlackNode<K,V> node) {
        return node != null && node.color == RED;
    }


    /* insert into tree */
    public void insert(K key, V val) {
        root = insert(root,key,val);
    }

    public RedBlackNode<K,V> insert(RedBlackNode<K,V> node, K key, V val) {
        if      (node == null)                { return new RedBlackNode<K,V>(key,val);       }

        if      (lesser(key, node.getKey()))  { node.left = insert(node.left, key, val);   }
        else if (greater(key, node.getKey())) { node.right = insert(node.right, key, val); }
        else                                  { node.val = val; }

        if (isRed(node.right) && !isRed(node.left))      { node = leftRotate(node);  }
        if (isRed(node.left)  &&  isRed(node.left.left)) { node = rightRotate(node); }
        if (isRed(node.left)  &&  isRed(node.right))     { flipColors(node);   }

        node.size = getSize(node.left) + getSize(node.right) + 1;
        return node;
    }


      /* search binary tree for key */
    public V search(K key) {
        return search(root, key);
    }

    private V search(RedBlackNode<K,V> node, K key) {

        if      (node == null)                { return null; }
        if      (lesser(key, node.getKey()))  { return search(node.left,key); }
        else if (greater(key, node.getKey())) { return search(node.right,key); }
        else                                  { return node.val; }
    }

    public int count() {
        return getSize(root);
    }

    /* sets value at node to be null */
    public void remove(K key) {
        remove(root, key);
    }

    private void remove(RedBlackNode<K,V> node, K key) {
        if (node == null ) return;

        if (node.getKey() == key)            { node.val = null; }
        else if (lesser(key, node.getKey())) { remove(node.left, key);  }
        else                                 { remove(node.right, key); }
    }

    public class TreeTraversal {

        /* Pre-order Traversal */
        public Iterable<V> preOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildPreOrder(root, q);
            return q;
        }

        private  void buildPreOrder(RedBlackNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            q.enqueue(node.val);
            buildPreOrder(node.left, q);
            buildPreOrder(node.right, q);
        }

        /* Post-order TraVersal */
        public  Iterable<V> postOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildPostOrder(root, q);
            return q;
        }

        private void buildPostOrder(RedBlackNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            buildPostOrder(node.left, q);
            buildPostOrder(node.right, q);
            q.enqueue(node.val);
        }

        /* In-order TraVersal */
        public Iterable<V> inOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildInOrder(root, q);
            return q;
        }

        private void buildInOrder(RedBlackNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            buildInOrder(node.left, q);
            q.enqueue(node.val);
            buildInOrder(node.right, q);
        }

    }

    public TreeTraversal traversal = new TreeTraversal();

    /* search for value <= key */
    public V floor(K key) {
        return floor(root, key);
    }

    private V floor(RedBlackNode<K, V> node, K key) {
        if (node == null) { return null; }

        if      (node.getKey() == key)        { return node.val; }
        else if (greater(node.getKey(), key)) { return floor(node.left, key); }
        else {
            V rightMin =  floor(node.right, key);
            if (rightMin == null) { return node.val; }
            else                  { return rightMin; }
        }
    }

    /* search for value => key */
    public V ceil(K key) {
        return ceil(root, key);
    }

    private V ceil(RedBlackNode<K, V> node, K key) {
        if (node == null) { return null; }

        if      (node.getKey() == key)        { return node.val; }
        else if (lesser(node.getKey(), key))  { return ceil(node.right, key); }
        else {
            V leftMax =  ceil(node.left, key);
            if (leftMax == null)  { return node.val; }
            else                  { return leftMax; }
        }
    }

    /* get rank of given key */
    public Integer rank(K key) {
        return rank(root, key) + 1;
    }

    private int rank(RedBlackNode<K, V> node, K key) {
        if (node == null) { return 0; }

        if      (node.getKey() == key)         { return getSize(node.left); }
        else if (lesser(key, node.getKey()))   { return rank(node.left, key);  }
        else                                   { return 1 + getSize(node.left) + rank(node.right, key); }
    }


}
