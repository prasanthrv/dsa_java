package com.dare.dsa.store;

import static com.dare.dsa.Util.lesser;

/* A minimum priority queue,
   using binary heap,
   the root will have the smallest element
*/
public class MinPQ<V extends Comparable> {
    private PQ<V> pq;

    public MinPQ(int capacity) {
        pq = new PQ<>(capacity, fn);
    }

    private CmpFn fn = new CmpFn() {
        @Override
        public <V extends Comparable> boolean cmpFn(V one, V two) {
            return lesser(one, two);
        }
    };

    public V delMin() {
        return pq.delTop();
    }

    public void insert(V item) {
        pq.insert(item);
    }

    public int size() {
        return pq.getSize();
    }

}
