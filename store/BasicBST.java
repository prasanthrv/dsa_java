package com.dare.dsa.store;

import com.dare.dsa.basic.Queue;

import static com.dare.dsa.Util.*;

/**
 * A normal Binary Search Tree implementation
 * @param <K> Key type
 * @param <V> Value type
 */
public class BasicBST<K extends Comparable, V> implements SymbolTable<K,V>{

    private BinaryNode<K,V> root;

    /* insert into tree */
    public void insert(K key, V val) {
        root = insert(root,key,val);
    }

    private BinaryNode<K,V> insert(BinaryNode<K,V> node, K key, V val) {

        if      (node == null)                { return new BinaryNode<K,V>(key,val);       }
        if      (lesser(key, node.getKey()))  { node.left = insert(node.left, key, val);   }
        else if (greater(key, node.getKey())) { node.right = insert(node.right, key, val); }
        else                                  { node.val = val; }

        node.size = BinaryNode.getSize(node.left) + BinaryNode.getSize(node.right) + 1;
        return node;
    }

    /* search binary tree for key */
    public V search(K key) {
        return search(root, key);
    }

    private V search(BinaryNode<K,V> node, K key) {

        if      (node == null)                { return null; }
        if      (lesser(key, node.getKey()))  { return search(node.left,key); }
        else if (greater(key, node.getKey())) { return search(node.right,key); }
        else                                  { return node.val; }
    }

    public int count() {
        return BinaryNode.getSize(root);
    }

    /* sets value at node to be null */
    public void remove(K key) {
        remove(root, key);
    }

    private void remove(BinaryNode<K,V> node, K key) {
        if (node == null ) return;

        if (node.getKey() == key)            { node.val = null; }
        else if (lesser(key, node.getKey())) { remove(node.left, key);  }
        else                                 { remove(node.right, key); }
    }

    public class TreeTraversal {

        /* Pre-order Traversal */
        public Iterable<V> preOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildPreOrder(root, q);
            return q;
        }

        private  void buildPreOrder(BinaryNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            q.enqueue(node.val);
            buildPreOrder(node.left, q);
            buildPreOrder(node.right, q);
        }

        /* Post-order TraVersal */
        public  Iterable<V> postOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildPostOrder(root, q);
            return q;
        }

        private void buildPostOrder(BinaryNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            buildPostOrder(node.left, q);
            buildPostOrder(node.right, q);
            q.enqueue(node.val);
        }

        /* In-order TraVersal */
        public Iterable<V> inOrderTraversal() {
            Queue<V> q = new Queue<>();
            buildInOrder(root, q);
            return q;
        }

        private void buildInOrder(BinaryNode<K,V> node, Queue<V> q) {
            if (node == null) return;

            buildInOrder(node.left, q);
            q.enqueue(node.val);
            buildInOrder(node.right, q);
        }

    }

    public TreeTraversal traversal = new TreeTraversal();

    /* search for value <= key */
    public V floor(K key) {
        return floor(root, key);
    }

    private V floor(BinaryNode<K, V> node, K key) {
        if (node == null) { return null; }

        if      (node.getKey() == key)        { return node.val; }
        else if (greater(node.getKey(), key)) { return floor(node.left, key); }
        else {
            V rightMin =  floor(node.right, key);
            if (rightMin == null) { return node.val; }
            else                  { return rightMin; }
        }
    }

    /* search for value => key */
    public V ceil(K key) {
        return ceil(root, key);
    }

    private V ceil(BinaryNode<K, V> node, K key) {
        if (node == null) { return null; }

        if      (node.getKey() == key)        { return node.val; }
        else if (lesser(node.getKey(), key))  { return ceil(node.right, key); }
        else {
            V leftMax =  ceil(node.left, key);
            if (leftMax == null)  { return node.val; }
            else                  { return leftMax; }
        }
    }

    /* get rank of given key */
    public Integer rank(K key) {
        return rank(root, key) + 1;
    }

    private int rank(BinaryNode<K, V> node, K key) {
        if (node == null) { return 0; }

        if      (node.getKey() == key)         { return BinaryNode.getSize(node.left); }
        else if (lesser(key, node.getKey()))   { return rank(node.left, key);  }
        else                                   { return 1 + BinaryNode.getSize(node.left) + rank(node.right, key); }
    }

}
