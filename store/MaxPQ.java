package com.dare.dsa.store;

import static com.dare.dsa.Util.greater;

/* A maximum priority queue,
   using binary heap,
   the root will have the largest element
 */
public class MaxPQ<V extends Comparable> {
    private PQ<V> pq;

    public MaxPQ(int capacity) {
        pq = new PQ<>(capacity, fn);
    }

    private CmpFn fn = new CmpFn() {
        @Override
        public <V extends Comparable> boolean cmpFn(V one, V two) {
            return greater(one, two);
        }
    };

    public V delMax() {
        return pq.delTop();
    }

    public void insert(V item) {
        pq.insert(item);
    }

    public int size() {
        return pq.getSize();
    }

}
