package com.dare.dsa.store;

import static com.dare.dsa.Util.swap;

/* An interface for the sorting function to compare value in PQ */
interface CmpFn {
   public <V extends Comparable> boolean cmpFn(V one, V two);
}

/* An implementation of a generic priority queue using binary heap */
class PQ<V extends Comparable>  {

    V[] list;
    int size;
    CmpFn cmpFn;

    public PQ(int capacity, CmpFn cmpFn) {
        list = (V[]) new Comparable[capacity];
        size = 0;
        this.cmpFn = cmpFn;
    }

    /* move current node to bottom, as long as cmpFn is true */
    private void sink(int index) {
       int left, right, succ;
       while (2*index <= size) {

           left = 2 * index;
           succ = left;
           right = left + 1;

           if (left < size)
            succ = cmpFn.cmpFn(list[left],list[right]) ? left: right;

           if (cmpFn.cmpFn(list[index], list[succ]))
               break;

           swap(list, index, succ);
           index = succ;
       }
    }

    /* move current node to top, as long as cmpFn is true */
    private void swim(int index) {
        int parent = index / 2;
        while (parent > 0 && cmpFn.cmpFn(list[index], list[parent])) {
            swap(list, parent, index);
            index = parent;
            parent = index /2;
        }
    }

    /* remove element at top and return it */
    V delTop() {
        V top = list[1];
        swap(list,1,size);
        list[size] = null;
        size--;
        sink(1);
        return top;
    }

    /* insert element in PQ */
    void insert(V item) {
        list[++size] = item;
        swim(size);
    }

    int getSize() {
        return size;
    }
}
