package com.dare.dsa.store;

import com.dare.dsa.basic.Stack;

import java.util.Iterator;

/* An ADT for adding items to a collection */
public class Bag<V> implements Iterable<V> {

    private Stack<V> st;

    public Bag() {
        st = new Stack<>();
    }

    public void add(V item) {
        st.push(item);
    }

    public boolean isEmpty() {
        return st.isEmpty();
    }

    public int size() {
        return st.size();
    }

    @Override
    public Iterator<V> iterator() {
        return st.iterator();
    }
}
