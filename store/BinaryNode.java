package com.dare.dsa.store;

/***
 * A binary node that has two pointers
 * left node  -> node has key lesser than parent
 * right node -> node has key greater than parent
 * size -> size of tree rooted at node
 * @param <K> Key
 * @param <V> Value
 */
public class BinaryNode<K extends Comparable, V> {
    private K key;
    V val;
    BinaryNode<K,V> left;
    BinaryNode<K,V> right;
    int size;

    public BinaryNode(K key, V val) {
        this.key = key;
        this.val = val;
        this.left = this.right = null;
        size = 1;
    }

    public BinaryNode(K key, V val, BinaryNode<K,V> left, BinaryNode<K,V> right) {
        this.key = key;
        this.val = val;
        this.left = left;
        this.right = right;
        size = 1;
    }

    public K getKey() {
        return key;
    }

    /* helper method for getting size, 0 if null */
    public static int getSize(BinaryNode node) {
        if (node == null) { return 0;         }
        else              { return node.size; }
    }
}
