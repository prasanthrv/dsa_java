package com.dare.dsa.store;

/* An interface for all Symbol Tables */
public interface SymbolTable<K extends Comparable, V> {
    public void insert(K key, V value);
    public V search(K key);
}
