package com.dare.dsa;

import static com.dare.dsa.Util.*;

public class Sorts {

    /* BubbleSort */
    public static void bubbleSort(Comparable[] items) {
        for (int i=0; i<items.length; i++) {
           for (int j=1; j< items.length - i; j++) {
               if (lesserOrEqual(items[j], items[j - 1]))
                   swap(items, j, j - 1);
           }
        }
    }

    /* SelectionSort */
    public static void selectionSort(Comparable[] items) {
        int min;

        for(int i =0 ;i < items.length; i++) {
            min = i;
            for (int j = i; j < items.length; j++ ) {
               if (lesserOrEqual(items[j], items[min]))
                   min = j;
            }
            if (min != i) { swap(items, i, min); };
        }
    }

    /* InsertionSort */
    public static void insertionSort(Comparable[] items) {
        for (int i = 0; i < items.length; i++) {
            for (int j = i; j > 0; j--) {
                if (lesserOrEqual(items[j], items[j - 1]))
                    swap(items, j, j- 1);
                else
                    break;
            }
        }
    }

    /* MergeSort */
    public static void mergeSort(Comparable[] items) {
        Comparable[] aux = new Comparable[items.length];
        mergeSort(items, aux, 0, items.length - 1);

    }

    private static void mergeSort(Comparable[] items, Comparable[] aux, int low, int high) {

        if (low >= high)
            return;

        int mid = low + (high - low) /2;
        mergeSort(items, aux, low, mid);
        mergeSort(items, aux, mid + 1, high);
        merge(items, aux, low, high);
    }

    private static void merge(Comparable[] items, Comparable[] aux, int low, int high) {
        int mid = low + (high - low) / 2;

        int i = low;
        int j = mid + 1;

        System.arraycopy(items,low,aux,low,high - low + 1);

        int k = low;
        while (k <= high) {
            if      (i > mid)                { items[k++] = aux[j++]; }
            else if (j > high)               { items[k++] = aux[i++]; }
            else if (lesserOrEqual(aux[i], aux[j])) { items[k++] = aux[i++]; }
            else                             { items[k++] = aux[j++]; }
        }

    }


    /* QuickSort */
    public static void quickSort(Comparable[] items) {
       quickSort(items, 0, items.length - 1);
    }

    private static void quickSort(Comparable[] items, int low, int high) {
        if (low >= high)
            return;

        int j = partition(items, low, high);
        quickSort(items, low, j-1);
        quickSort(items, j+1, high);
    }

    private static int partition(Comparable[] items, int low, int high) {

        Comparable pivot = items[low];
        int i = low;
        int j = high;

        while(true) {
            while (j > low && !lesserOrEqual(items[j], pivot))  { j--; }
            while (i < high && lesserOrEqual(items[i], pivot)) { i++; }

            if (i >= j)
                break;

            swap(items, i, j);
        }
        swap(items, low, j);
        return j;
    }

    /* get the k-th ranked item in an array,
        uses QuickSelect, a modification of QuickSort
     */
    public static Comparable getRankedItem(Comparable[] items, int rank) {

        rank = rank - 1;
        int j, low = 0, high = items.length - 1;

        while(low <= high) {
            j = partition(items, low, high);

            if (rank < j)       { high = j - 1; }
            else if (rank > j)  { low = j + 1;  }
            else                { return items[j]; }

        }
        return null;
    }

    /* get the rank of an item in an array */
    public static <T extends Comparable> int getRank(T[] items, T item) {
        int smaller = 0;
        boolean found = false;
        for (T cur : items) {
            if (cur.compareTo(item) < 0) { smaller++;    }
            if (cur == item)             { found = true; }
        }
        if (found) { return smaller + 1; }
        else       { return -1;          }
    }


    /*  remove duplicate elements in an array,
        uses a modification of MergeSort,
        returns the no of unique elements in modified array, from [0..unique_mark]
     */
    public static int uniqueElems(Comparable[] items) {
        Comparable[] aux = new Comparable[items.length];
        int removedItems = unqMSort(items, aux, 0, items.length - 1);
        return items.length - removedItems - 1;

    }

    private static int unqMSort(Comparable[] items, Comparable[] aux, int low, int high) {

        if (low >= high)
            return 0;

        int mid = low + (high - low) /2;
        unqMSort(items, aux, low, mid);
        unqMSort(items, aux, mid + 1, high);
        return unqMerge(items, aux, low, high);
    }

    private static int unqMerge(Comparable[] items, Comparable[] aux, int low, int high) {
        int mid = low + (high - low) / 2;

        int i = low;
        int j = mid + 1;

        System.arraycopy(items,low,aux,low,high - low + 1);

        int rm = 0;
        int k = low;
        while (k <= high - rm) {
            while (i < mid && equal(aux[i],aux[i+1]))  { i++; rm++; }
            while (j < high && equal(aux[j],aux[j+1])) { j++; rm++; }

            if      (i > mid)                 { items[k++] = aux[j++]; }
            else if (j > high)                { items[k++] = aux[i++]; }
            else if (lesser(aux[i], aux[j]))  { items[k++] = aux[i++]; }
            else if (greater(aux[i], aux[j])) { items[k++] = aux[j++]; }
            else if (equal(aux[i], aux[j]))   { items[k++] = aux[i++]; j++; rm++; }
        }

        for (int p = k; p <= high; p++ )
            items[p] = Integer.MAX_VALUE;

        return rm;

    }
}
